package com.bellatorhd.trm;

import com.bellatorhd.trm.blocks.TRMBlocks;

import com.bellatorhd.trm.entities.TRMEntities;
import com.bellatorhd.trm.items.TRMItems;
import com.bellatorhd.trm.setup.ModSetup;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.EventPriority;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


@Mod(Rickstaverse.MODID)
public class Rickstaverse
{

    private static final Logger LOGGER = LogManager.getLogger();

    public static ModSetup setup = new ModSetup();

    public static final String MODID = "trm";



    public Rickstaverse() {
        FMLJavaModLoadingContext.get().getModEventBus().register(this);
        MinecraftForge.EVENT_BUS.register(this);


        FMLJavaModLoadingContext.get().getModEventBus().addListener(ModSetup::init);
    }

    @OnlyIn(Dist.CLIENT)
    @SubscribeEvent
    public void setup(final FMLClientSetupEvent event) {
        TRMEntities.initRenderers();
    }

    @SubscribeEvent(priority = EventPriority.LOWEST)
    public void registries(RegistryEvent.NewRegistry e){

        TRMItems.ITEMS.register(FMLJavaModLoadingContext.get().getModEventBus());
        TRMBlocks.BLOCKS.register(FMLJavaModLoadingContext.get().getModEventBus());
        TRMEntities.ENTITY_TYPES.register(FMLJavaModLoadingContext.get().getModEventBus());

    }


}
