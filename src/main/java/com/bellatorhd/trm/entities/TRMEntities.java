package com.bellatorhd.trm.entities;


import com.bellatorhd.trm.Rickstaverse;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityClassification;
import net.minecraft.entity.EntityType;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;


import java.util.function.Supplier;


@Mod.EventBusSubscriber(modid = Rickstaverse.MODID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class TRMEntities {

    public static final DeferredRegister<EntityType<?>> ENTITY_TYPES = new DeferredRegister<>(ForgeRegistries.ENTITIES, Rickstaverse.MODID);

    //public static final RegistryObject<EntityType<PortalProjectile>> PORTAL_PROJECTILE = register("portal_projectile", () -> EntityType.Builder.<PortalProjectile>create(PortalProjectile::new, EntityClassification.MISC).size(0.5F, 0.5F));

    @OnlyIn(Dist.CLIENT)
    public static void initRenderers() {
        //RenderingRegistry.registerEntityRenderingHandler(PORTAL_PROJECTILE.get(), ProjectileRenderer::new);

    }

    public static <T extends Entity> RegistryObject<EntityType<T>> register(String id, Supplier<EntityType.Builder<T>> builderSupplier) {
        return ENTITY_TYPES.register(id, () -> builderSupplier.get().build(Rickstaverse.MODID + ":" + id));
    }

}
