package com.bellatorhd.trm.blocks;

import com.bellatorhd.trm.Rickstaverse;
import com.bellatorhd.trm.items.TRMItems;
import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraftforge.common.ToolType;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

import java.util.function.Supplier;

public class TRMBlocks {

    public static final DeferredRegister<Block> BLOCKS = new DeferredRegister<>(ForgeRegistries.BLOCKS, Rickstaverse.MODID);


    //Ores
    public static final RegistryObject<Block> ISOTOPE_ORE = register("isotope_ore", () -> new Block(Block.Properties.create(Material.ROCK).harvestTool(ToolType.PICKAXE).harvestLevel(2).hardnessAndResistance(3.0F, 5.0F)));

    //Quantum Blocks that I will remove when Lucas steals

    public static final RegistryObject<Block> QUANTUM_BLOCK = register("quantum_block", () -> new Block(Block.Properties.create(Material.EARTH).sound(SoundType.SAND).harvestTool(ToolType.SHOVEL).harvestLevel(2).hardnessAndResistance(1.5F, 2.0F)));
    public static final RegistryObject<Block> QUANTUM_SOIL = register("quantum_soil", () -> new Block(Block.Properties.create(Material.SAND).sound(SoundType.SAND).harvestTool(ToolType.SHOVEL).harvestLevel(1).hardnessAndResistance(1.5F, 2.0F)));
    public static final RegistryObject<Block> QUANTUM_STONE = register("quantum_stone", () -> new Block(Block.Properties.create(Material.ROCK).harvestTool(ToolType.PICKAXE).harvestLevel(2).hardnessAndResistance(3.0F, 5.0F)));


    public static <T extends Block> RegistryObject<T> register(String id, Supplier<T> blockSupplier) {
        RegistryObject<T> registryObject = BLOCKS.register(id, blockSupplier);
        TRMItems.ITEMS.register(id, () -> new BlockItem(registryObject.get(), new Item.Properties().group(ItemGroup.BUILDING_BLOCKS)));
        return registryObject;
    }


}

