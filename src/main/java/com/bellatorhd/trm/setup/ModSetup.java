package com.bellatorhd.trm.setup;

import com.bellatorhd.trm.Rickstaverse;
import com.bellatorhd.trm.blocks.TRMBlocks;
import com.bellatorhd.trm.networking.Networking;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;

@Mod.EventBusSubscriber(modid = Rickstaverse.MODID, bus = Mod.EventBusSubscriber.Bus.FORGE)
public class ModSetup {

    public static void init(final FMLCommonSetupEvent event) {
        Networking.registerMessages();
    }


}
