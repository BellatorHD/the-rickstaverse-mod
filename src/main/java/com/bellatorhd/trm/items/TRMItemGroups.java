package com.bellatorhd.trm.items;

import com.bellatorhd.trm.blocks.TRMBlocks;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;

public class TRMItemGroups {

    public static final ItemGroup rickstaverse = new ItemGroup("trm") {
        @Override
        public ItemStack createIcon() {
            return new ItemStack(TRMBlocks.ISOTOPE_ORE.get());
        }
    };
}