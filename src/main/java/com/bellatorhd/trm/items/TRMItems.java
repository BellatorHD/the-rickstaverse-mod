package com.bellatorhd.trm.items;

import com.bellatorhd.trm.Rickstaverse;
import com.bellatorhd.trm.setup.ModSetup;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

public class TRMItems {

    public static final DeferredRegister<Item> ITEMS = new DeferredRegister<>(ForgeRegistries.ITEMS, Rickstaverse.MODID);

    //Misc Items
    public static final RegistryObject<Item> PORTAL_GUN = ITEMS.register("portal_gun", () -> new PortalGunItem(new Item.Properties().group(TRMItemGroups.rickstaverse).maxStackSize(1)));
    public static final RegistryObject<Item> PORTALFLUID_BUCKET = ITEMS.register("portalfluid_bucket", () -> new Item(new Item.Properties().group(ItemGroup.MISC).maxStackSize(1)));
    public static final RegistryObject<Item> MEESEEKS_BOX = ITEMS.register("meeseeks_box", () -> new Item(new Item.Properties().group(TRMItemGroups.rickstaverse).maxStackSize(1)));
    public static final RegistryObject<Item> GUN_CASING = ITEMS.register("gun_casing", () -> new Item(new Item.Properties().group(ItemGroup.MISC).maxStackSize(16)));
    public static final RegistryObject<Item> GLASS_TUBE = ITEMS.register("glass_tube", () -> new Item(new Item.Properties().group(ItemGroup.MISC).maxStackSize(16)));
    public static final RegistryObject<Item> PORTALFLUID_TUBE = ITEMS.register("portalfluid_tube", () -> new Item(new Item.Properties().group(ItemGroup.MISC).maxStackSize(16)));

    //Nuggets
    public static final RegistryObject<Item> ISOTOPE_NUGGET = ITEMS.register("isotope_nugget", () -> new Item(new Item.Properties().group(ItemGroup.MATERIALS)));
}
